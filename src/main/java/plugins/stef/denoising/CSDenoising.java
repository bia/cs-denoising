package plugins.stef.denoising;

import java.io.IOException;
import java.net.URL;

import icy.file.FileUtil;
import icy.gui.dialog.MessageDialog;
import icy.gui.frame.progress.AnnounceFrame;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import icy.system.IcyHandledException;
import icy.system.thread.ThreadUtil;
import icy.type.DataType;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarEnum;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.ylemontag.matlabfunctioncaller.ArgumentSetInput;
import plugins.ylemontag.matlabfunctioncaller.ArgumentSetOutput;
import plugins.ylemontag.matlabfunctioncaller.MatlabFunctionCaller;
import plugins.ylemontag.matlabfunctioncaller.MatlabFunctionCaller.ExecutionFailed;
import plugins.ylemontag.matlabfunctioncaller.MatlabFunctionCaller.Options;
import plugins.ylemontag.matlabfunctioncaller.Variant;

public class CSDenoising extends EzPlug implements Block
{
    public enum SamplingMode
    {
        UNI_FC
        {
            @Override
            public String getId()
            {
                return "uni_fc";
            }
        },
        GAUSSIAN
        {
            @Override
            public String getId()
            {
                return "gaussian";
            }
        },
        STAR
        {
            @Override
            public String getId()
            {
                return "star";
            }
        };

        public String getId()
        {
            return name();
        }
    }

    public enum RegularizationMode
    {
        L1
        {
            @Override
            public String getId()
            {
                return "l1";
            }
        },
        TV
        {
            @Override
            public String getId()
            {
                return "tv";
            }
        };

        public String getId()
        {
            return name();
        }
    }

    // input variables
    protected EzVarSequence inputImage;
    protected EzVarEnum<SamplingMode> samplingMode;
    protected EzVarDouble samplingRate;
    protected EzVarInteger nbReconstruction; // > 0
    protected EzVarEnum<RegularizationMode> regularizationMode;
    protected EzVarDouble noiseStdDev;
    protected EzVarDouble frequencyCut;
    protected EzVarInteger nbBeams;
    protected EzVarInteger theta;

    // output variables
    protected EzVarSequence outputImage; // adaptive image
    protected EzVarSequence varianceMap; // variance map

    // internal
    protected MatlabFunctionCaller functionCaller;

    public CSDenoising()
    {
        super();

        functionCaller = null;

        // declare variables here (better for protocol)
        inputImage = new EzVarSequence("Input sequence");

        samplingMode = new EzVarEnum<SamplingMode>("Sampling method", SamplingMode.values(), SamplingMode.GAUSSIAN);
        samplingRate = new EzVarDouble("Sampling rate", 0.33d, 0.0001d, 1, 0.01d);
        nbReconstruction = new EzVarInteger("Nb reconstruction", 3, 1, Integer.MAX_VALUE, 1);
        regularizationMode = new EzVarEnum<RegularizationMode>("Regularization method", RegularizationMode.values(),
                RegularizationMode.L1);
        noiseStdDev = new EzVarDouble("Noise standard deviation", 0.10d, 0.0001d, 1, 0.01d);

        // sampling mode = UNI_FC only
        frequencyCut = new EzVarDouble("Frequency cut (0 = auto)", 0d, 0d, 1, 0.01d);
        // sampling mode = STAR only
        nbBeams = new EzVarInteger("Nb beams", 16, 0, 64, 2);
        // sampling mode = STAR only
        theta = new EzVarInteger("Theta (in degree)", 0, 0, 360, 15);

        outputImage = new EzVarSequence("Result"); // adaptive // image
        varianceMap = new EzVarSequence("Variance map"); // variance
    }

    public MatlabFunctionCaller getMatlabFunctionCaller(String resourceName) throws IOException
    {
        final Options options = MatlabFunctionCaller.getOptions();

        // wait for validation
        final AnnounceFrame waitValid = new AnnounceFrame("Checking Matlab...");
        try
        {
            while (options.isValid() == null)
                ThreadUtil.sleep(10);
        }
        finally
        {
            waitValid.close();
        }

        // Matlab is not properly configured ?
        if (!options.isValid())
            throw new IcyHandledException(
                    "Plugin MatlabFunctionCaller isn't properly configured, open it and set the matlab binary path.");

        final String baseFolder = FileUtil.getTempDirectory() + FileUtil.separator + "cs_denoising"
                + FileUtil.separator;

        // extract all MATLAB resources
        if (!extractMatlabResources(baseFolder))
            return null;

        // initialize function caller
        return new MatlabFunctionCaller(baseFolder + "cs_denoising.m");
    }

    private boolean extractMatlabResources(String baseFolder) throws IOException
    {
        // IMPORTANT: file/resource must have the same name as the function !
        return extractSingleResource(baseFolder, "plugins/stef/denoising/matlab/cs_denoising.m")
                && extractSingleResource(baseFolder + "NESTA/", "plugins/stef/denoising/matlab/NESTA/Setup_Nesta.m")
                && extractSingleResource(baseFolder + "NESTA/", "plugins/stef/denoising/matlab/NESTA/NESTA.m")
                && extractSingleResource(baseFolder + "NESTA/", "plugins/stef/denoising/matlab/NESTA/NESTA_UP.m")
                && extractSingleResource(baseFolder + "NESTA/",
                "plugins/stef/denoising/matlab/NESTA/fastProjection.m")
                && extractSingleResource(baseFolder + "NESTA/",
                "plugins/stef/denoising/matlab/NESTA/Core_Nesterov.m")
                && extractSingleResource(baseFolder + "NESTA/",
                "plugins/stef/denoising/matlab/NESTA/Core_Nesterov_UP.m")
                &&
                // NESTA solver
                extractSingleResource(baseFolder + "NESTA/RecPF/solver/",
                        "plugins/stef/denoising/matlab/NESTA/RecPF/solver/RecPF_Modified.m")
                && extractSingleResource(baseFolder + "NESTA/RecPF/solver/",
                "plugins/stef/denoising/matlab/NESTA/RecPF/solver/RecPF.m")
                && extractSingleResource(baseFolder + "NESTA/RecPF/solver/",
                "plugins/stef/denoising/matlab/NESTA/RecPF/solver/RecPF.asv")
                &&
                // NESTA utilities
                extractSingleResource(baseFolder + "NESTA/RecPF/utilities/",
                        "plugins/stef/denoising/matlab/NESTA/RecPF/utilities/dctPhi.m")
                && extractSingleResource(baseFolder + "NESTA/RecPF/utilities/",
                "plugins/stef/denoising/matlab/NESTA/RecPF/utilities/funcval.m")
                && extractSingleResource(baseFolder + "NESTA/RecPF/utilities/",
                "plugins/stef/denoising/matlab/NESTA/RecPF/utilities/identityPhi.m")
                && extractSingleResource(baseFolder + "NESTA/RecPF/utilities/",
                "plugins/stef/denoising/matlab/NESTA/RecPF/utilities/MRImask.m")
                && extractSingleResource(baseFolder + "NESTA/RecPF/utilities/",
                "plugins/stef/denoising/matlab/NESTA/RecPF/utilities/selectPF.m")
                && extractSingleResource(baseFolder + "NESTA/RecPF/utilities/",
                "plugins/stef/denoising/matlab/NESTA/RecPF/utilities/snr.m")
                && extractSingleResource(baseFolder + "NESTA/RecPF/utilities/",
                "plugins/stef/denoising/matlab/NESTA/RecPF/utilities/Wavedb1Phi.m")
                &&
                // NESTA misc
                extractSingleResource(baseFolder + "NESTA/Misc/", "plugins/stef/denoising/matlab/NESTA/Misc/A_f.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/A_fhp.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/A_Subset.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/At_f.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/At_fhp.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/At_Subset.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/calctv.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/CGwrapper.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/counter.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/LineMask.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/LSQRwrapper.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/MakeRDSquares.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/msp_signal.m")
                && extractSingleResource(baseFolder + "NESTA/Misc/",
                "plugins/stef/denoising/matlab/NESTA/Misc/my_normest.m")
                &&
                // NESTA analysis
                extractSingleResource(baseFolder + "NESTA/Analysis/",
                        "plugins/stef/denoising/matlab/NESTA/Analysis/hadamard.c")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/hadamard.m")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/hadamard.mexa64")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/hadamard.mexglx")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/hadamard.mexmac")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/hadamard.mexmaci")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/hadamard.mexmaci64")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/hadamard.mexw32")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/make_window.m")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/makeSnr.m")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/PsiTransposeWFF.m")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/PsiWFF.m")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/pulseTrain.m")
                && extractSingleResource(baseFolder + "NESTA/Analysis/",
                "plugins/stef/denoising/matlab/NESTA/Analysis/radar_signal.m");
    }

    private boolean extractSingleResource(String destFolder, String resourceName) throws IOException
    {
        // get matlab file resource URL
        final URL matURL = getResource(resourceName);

        if (matURL == null)
            throw new IOException("Cannot find resource: " + resourceName);

        // extract resource (IMPORTANT: file/resource must have the same name as
        // the function)
        return extractResource(destFolder + FileUtil.getFileName(resourceName, true), matURL) != null;
    }

    @Override
    protected void initialize()
    {
        // make interface visible
        addEzComponent(inputImage);
        addEzComponent(samplingMode);
        addEzComponent(samplingRate);
        addEzComponent(nbReconstruction);
        addEzComponent(regularizationMode);
        addEzComponent(noiseStdDev);
        addEzComponent(frequencyCut);
        addEzComponent(nbBeams);
        addEzComponent(theta);
    }

    @Override
    public void clean()
    {
        // remove temp folder / files we created
        FileUtil.delete(FileUtil.getTempDirectory() + FileUtil.separator + "cs_denoising", true);
    }

    @Override
    protected void execute()
    {
        try
        {
            if (functionCaller == null)
                functionCaller = getMatlabFunctionCaller("plugins/stef/denoising/matlab/cs_denoising.m");
        }
        catch (IOException e)
        {
            throw new IcyHandledException(
                    "CSDenoising error: Matlab function caller isn't initialized, can't continue !", e);
        }

        Sequence seq = inputImage.getValue();

        if (seq == null)
        {
            if (!isHeadLess())
                MessageDialog.showDialog("No input image !", MessageDialog.ERROR_MESSAGE);

            System.err.println("CSDenoising error: no input image !");

            // null result
            outputImage.setValue(null);
            varianceMap.setValue(null);

            return;
        }

        final DataType dataType = seq.getDataType_();

        // we need to convert to double
        if (dataType.isInteger())
            seq = SequenceUtil.convertToType(seq, DataType.DOUBLE, true);

        /*
         * function [res,var] = cs_denoising(noisy,sampling_mode,sampling_rate,
         * nb_reconstructions,noise_std,regularization_mode,freq_cut,nb_beams,
         * theta0)
         */

        final ArgumentSetInput argsIn = functionCaller.getInputArguments();

        // bind all input arguments
        argsIn.bind("noisy", new Variant(seq));
        argsIn.bind("sampling_mode", new Variant(samplingMode.getValue().getId()));
        argsIn.bind("sampling_rate", new Variant(samplingRate.getValue()));
        argsIn.bind("nb_reconstructions", new Variant((double) nbReconstruction.getValue()));
        argsIn.bind("noise_std", new Variant(noiseStdDev.getValue()));
        argsIn.bind("regularization_mode", new Variant(regularizationMode.getValue().getId()));
        argsIn.bind("freq_cut", new Variant(frequencyCut.getValue()));
        argsIn.bind("nb_beams", new Variant((double) nbBeams.getValue()));
        argsIn.bind("theta0", new Variant(Math.toRadians(theta.getValue())));

        try
        {
            // execute matlab function
            functionCaller.execute();
        }
        catch (ExecutionFailed e)
        {
            throw new IcyHandledException("Error while executing matlab code: " + e.getMatlabMessage());
        }
        catch (Exception e)
        {
            throw new IcyHandledException(e);
        }

        final ArgumentSetOutput argsOut = functionCaller.getOutputArguments();

        // get results
        Sequence denoisedImg = argsOut.getArgument("res").getAsSequence();
        final Sequence varMapImg = argsOut.getArgument("var").getAsSequence();

        // convert back to original data type
        if (!denoisedImg.getDataType_().equals(dataType))
            denoisedImg = SequenceUtil.convertToType(denoisedImg, dataType, true);

        denoisedImg.setName(inputImage.getValue().getName() + " - CS denoised");
        varMapImg.setName(inputImage.getValue().getName() + " - CS denoised - variance map");

        // make result visible if no executing from protocol
        if (!isHeadLess())
        {
            addSequence(denoisedImg);
            addSequence(varMapImg);
        }

        // set in result variables
        outputImage.setValue(denoisedImg);
        varianceMap.setValue(varMapImg);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", inputImage.getVariable());
        inputMap.add("samplingMode", samplingMode.getVariable());
        inputMap.add("samplingRate", samplingRate.getVariable());
        inputMap.add("nbReconstruction", nbReconstruction.getVariable());
        inputMap.add("regularizationMode", regularizationMode.getVariable());
        inputMap.add("noiseStdDev", noiseStdDev.getVariable());
        inputMap.add("frequencyCut", frequencyCut.getVariable());
        inputMap.add("nbBeams", nbBeams.getVariable());
        inputMap.add("theta", theta.getVariable());
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("outputImage", outputImage.getVariable());
        outputMap.add("varianceMap", varianceMap.getVariable());
    }
}
