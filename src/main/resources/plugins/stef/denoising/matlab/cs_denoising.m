function [res,var] = cs_denoising(noisy,sampling_mode,sampling_rate, nb_reconstructions,noise_std,regularization_mode,freq_cut,nb_beams,theta0)

% CS_based denoising method using multiple CS reconstructions
%
% OUTPUTS:
%  - res => structure containing :
%        - img_mean => Denoised image by averaging the CS reconstructions.
%        - variance_map => Normalized variance map of the series of reconstructions.
%        - img_adaptive => Denoised image by adaptive reconstruction.
%        - img_fba => Denoised image using Fourier Burst Accumulation method.
%        - img_ewa => Denoised image using Exponential Weighted Averaging method.
%        - img_series => Series of partial reconstructions.
%
% INTPUTS:
%  - noisy => Noisy image to denoise.
%  - sampling_mode => Name of the sampling mode to use (see
%  make_sampling_mask_fourier.m). Should be a string.
%  - sampling_rate => Rate of data coefficients that are used for the
%  reconstruction (should be between 0 and 1).
%  - noise_std => Standard deviation of the level of noise in the image.
%  - regularization_mode => Regularization mode for the CS-optimization.
%  - varargin => additional parameter depending on the sampling mode.
%
% AUTHOR: William Meiniel (william.meiniel@pasteur.fr)
%

% setup path for NESTA -
% This adds an absolute path, so it works
% when the user is in any directory

p = mfilename('fullpath');   % the location of this file
path = fileparts(p);
addpath(fullfile(path, 'NESTA'));
addpath(fullfile(path, 'NESTA', 'RecPF', 'solver'));
addpath(fullfile(path, 'NESTA', 'RecPF', 'utilities'));
addpath(fullfile(path, 'NESTA', 'Misc'));
addpath(fullfile(path, 'NESTA', 'Analysis'));

% can start the function now =)

n1 = size(noisy,1);
n2 = size(noisy,2);

% Parameters for the CS reconstruction

cs_options = struct();
cs_options.tolvar = 1e-5;

%% Multiple-CS denoising

% Compute the optimal size of the full sampled area for 'uni_fc' sampling

if (strcmpi(sampling_mode, 'uni_fc'))
    if freq_cut == 0
        % Implement the periodic+smooth decomposition
        [p,~] = perdecomp(noisy);
        
        % Set the size of the disc so that 99.9% of the energy is contained
        % inside
        
        fourier_p = fftshift(fft2(p));
        [X, Y] = meshgrid(1:n2, 1:n1);
        lambda = 0 ;
        energy = fourier_p(n2/2,n1/2) ;
        energy_total = norm(fourier_p,2);
        
        while (energy < 0.999*energy_total && lambda <=1 )
            lambda = lambda + 0.025 ;
            disc = hypot((X-n1/2)/(lambda*n1), (Y-n2/2)/(lambda*n2)) <= 1 ;
            energy = norm(double(disc).*fourier_p,2) ;
        end
        
        freq_cut = lambda ;
    end
elseif (strcmpi(sampling_mode, 'star'))
    freq_cut = 0;
end

% Generate the random sampling masks.

cs_M    = round(numel(noisy) * sampling_rate);
cs_mask = false(n1, n2, nb_reconstructions);

if (strcmpi(sampling_mode, 'uni_fc'))
    
    for k=1:nb_reconstructions
        cs_mask(:,:,k) = make_sampling_mask_fourier(n1, n2, sampling_mode, cs_M,freq_cut);
    end
    
elseif (strcmpi(sampling_mode, 'gaussian'))
    for k=1:nb_reconstructions
        cs_mask(:,:,k) = make_sampling_mask_fourier(n1, n2, sampling_mode, cs_M);
    end
    
elseif (strcmpi(sampling_mode, 'star'))
    
    for k=1:nb_reconstructions
        cs_mask(:,:,k) = make_sampling_mask_fourier(n1, n2, sampling_mode, cs_M,freq_cut,nb_beams,theta0);
    end
end
% Perform the individual CS reconstructions.
img_series   = zeros(n1, n2, nb_reconstructions);

for k=1:nb_reconstructions
		% Info
			%fprintf('Reconstruction %d/%d \n', k, nb_reconstructions);

			% CS parameters
			current_mask    = cs_mask(:,:,k);
			current_M       = numel(find(current_mask));
			current_epsilon = noise_std * sqrt(current_M + 2*sqrt(2*current_M));
			A  = @(x) partial_fft2(x, current_mask);
			At = @(y) real(partial_ifft2(y, current_mask));
			b  = A(noisy);
			
			% Call the reconstruction method.
			img_series(:,:,k) = cs_reconstruction_2d(A, At, b, n1, n2, ...
				current_epsilon, regularization_mode, cs_options);
end

% res.img_series = img_series ;

%% Fusion by averaging and variance map

img_mean         = squeeze(mean(img_series, 3));

% res.img_mean     = img_mean;

if nb_reconstructions > 1
    
    variance_map = squeeze(std(img_series,0,3));
    variance_map = variance_map-min(variance_map(:));
    variance_map = variance_map/max(variance_map(:));
    
    %variance_map = variance_map .* (variance_map >= 0.1) ;
    
    % res.variance_map = variance_map ;
    var = variance_map;
    
    %% Adaptive reconstruction
    
    J = roifilt2([1/16 1/8 1/16 ; 1/8 1/4 1/8 ; 1/16 1/8 1/16], noisy,ones(size(noisy))); % This can be put as a parameter
    
    img_adaptive = (variance_map .* J) + (1-variance_map) .* img_mean ;
    % res.img_adaptive = img_adaptive ;
    res = img_adaptive ;
    
    %% EWA reconstruction
    
    beta = 0.01 ;
    Observation = noisy/max(noisy(:)) ;
    img_ewa = zeros(n1,n2);
    
    for i=1:n1
        for j=1:n2
            % Original noisy observation
            Y           = Observation(i,j);
            rep_Y       = repmat(Y,nb_reconstructions,1);
            
            % Estimators
            fhat        = squeeze(img_series(i,j,:));
            
            % Residu of estimates
            rhat = (sum(abs( rep_Y-fhat).^2,2)./nb_reconstructions)*nb_reconstructions;
            rhat = max(rhat(:))-rhat;
            
            % Aggregation of estimators
            [denoised_EWA,~] = aggreg_EWA_adapted(fhat,rhat,beta);
            img_ewa(i,j)              = denoised_EWA;
        end
    end
    
    % res.img_ewa = img_ewa ;
    
    %% FBA reconstruction
    
    % Initialization
    
    p = 25;
    img_av_ift = zeros(n1,n2);
    H = fspecial('gaussian',3,min(n1,n2)/50);
    img_ift = zeros(n1,n2);
    img_ift_smooth = zeros(n1,n2);
    
    for k=1:nb_reconstructions
        img_ift(:,:,k) = fft2(img_series(:,:,k));
        img_ift_smooth(:,:,k) = imfilter(abs(img_ift(:,:,k)), H);
        img_av_ift     = img_av_ift+(img_ift(:,:,k).*(img_ift_smooth(:,:,k).^p));
    end
    
    img_av_ift = img_av_ift./sum(img_ift_smooth.^p,3);
    img_fba         = real(ifft2(img_av_ift));
    %img_fba(img_fba(:)<0)=0;
    
    % res.img_fba = img_fba ;
    
    %% Random combination
    
    img_rand = zeros(n1,n2) ;
    
    for i=1:n1
        for j=1:n2
            weights = rand(nb_reconstructions,1) ;
            weights = weights/sum(weights) ;
            img_rand(i,j) = sum(weights.*squeeze(img_series(i,j,:))) ;
        end
    end
    
    % res.img_rand = img_rand ;
    
    
else
    % res.variance_map = 0*img_mean ;
    % res.img_adaptive = img_mean ;
    % res.img_fba = img_mean ;
    % res.img_ewa = img_mean ;
    
    res = img_mean;
    var = 0*img_mean;
end


%% List of functions


function [p,s] = perdecomp(u)
%------------------------------ PERDECOMP ------------------------------

%       Periodic plus Smooth Image Decomposition
%
%               author: Lionel Moisan
%
%   This program is freely available on the web page
%
%   http://www.mi.parisdescartes.fr/~moisan/p+s
%
%   I hope that you will find it useful.
%   If you use it for a publication, please mention 
%   this web page and the paper it makes reference to.
%   If you modify this program, please indicate in the
%   code that you did so and leave this message.
%   You can also report bugs or suggestions to 
%   lionel.moisan [AT] parisdescartes.fr
%
% This function computes the periodic (p) and smooth (s) components
% of an image (2D array) u
%
% usage:    p = perdecomp(u)    or    [p,s] = perdecomp(u)
%
% note: this function also works for 1D signals (line or column vectors)
%
% v1.0 (01/2014): initial Matlab version from perdecomp.sci v1.2

[ny,nx] = size(u); 
u = double(u);
X = 1:nx; Y = 1:ny;
v = zeros(ny,nx);
v(1,X)  = u(1,X)-u(ny,X);
v(ny,X) = -v(1,X);
v(Y,1 ) = v(Y,1 )+u(Y,1)-u(Y,nx);
v(Y,nx) = v(Y,nx)-u(Y,1)+u(Y,nx);
fx = repmat(cos(2.*pi*(X -1)/nx),ny,1);
fy = repmat(cos(2.*pi*(Y'-1)/ny),1,nx);
fx(1,1)=0.;   % avoid division by 0 in the line below
s = real(ifft2(fft2(v)*0.5./(2.-fx-fy)));
p = u-s;

end

function y = partial_fft2(x, mask)
% PARTIAL_FFT2 Apply a partial Fourier transform to a 2D image.
%
% y = PARTIAL_FFT2(x, mask)
%
% INPUTS:
%  - x => a 2D image.
%  - mask => a boolean mask with the same dimensions as 'x', indicating the
%    position of the samples in the Fourier domain. The mask quadrants are
%    supposed shifted so that the DC component of the Fourier domain corresponds
%    to 'mask(1,1)'.
%
% OUTPUTS:
%  - y => column vector containing the selected Fourier coefficients.
%
% If 'x' has more than 2 dimensions, a partial 2D Fourier transform is applied to
% each 2D slice of 'x'. Anyway, 'mask' must have the same size than 'x'.
%
% AUTHOR: Yoann Le Montagner  (yoann.le-montagner -at- m4x.org)
%
%
% [1] Y. Le Montagner, "Algorithmic solutions toward applications of compressed
%     sensing for optical imaging", Telecom ParisTech, 2013.


% Image dimensions
n1 = size(mask, 1);
n2 = size(mask, 2);

% Partial FT
ft = fft2(x) / sqrt(n1*n2);
y  = ft(mask);
end


function x = partial_ifft2(y, mask)
% PARTIAL_IFFT2 Apply the adjoint of a partial 2D Fourier transform.
% 
% x = PARTIAL_IFFT2(y, mask)
%
% INPUTS:
%  - y => column vector containing the Fourier samples.
%  - mask => a 2D boolean mask indicating the position of the samples in the
%    Fourier domain.
%
% OUTPUTS:
%  - x => 2D image obtained from the Fourier samples 'y', assuming that the
%    missing coefficients are zero.
%
% Remark: the normalization factor 'sqrt(n1*n2)' is important to ensure that
% this function is actually the algebraic adjoint of 'partial_fft2'.
%
% If 'mask' has more than 2 dimensions, the behavior of 'partial_ifft2' is coherent
% with the one of 'partial_fft2': each 2D slice is processed separately.
% Anyway, 'x' always have the same size than 'mask'.
%
% AUTHOR: Yoann Le Montagner  (yoann.le-montagner -at- m4x.org)
%
%
% [1] Y. Le Montagner, "Algorithmic solutions toward applications of compressed
%     sensing for optical imaging", Telecom ParisTech, 2013.


% Image dimensions
n1 = size(mask, 1);
n2 = size(mask, 2);

% Inverse partial FT
ft = zeros(size(mask)); ft(mask) = y;
x  = ifft2(ft) * sqrt(n1*n2);
end


function [mask, actual_M] = make_sampling_mask_fourier(n1, n2, sampling_mode, M, varargin)
% MAKE_SAMPLING_MASK_FOURIER Generate a mask to perform sampling in the Fourier domain.
% 
% [mask, actual_M] = MAKE_SAMPLING_MASK_FOURIER(n1, n2, sampling_mode, M, ...)
%
% This function generates a sampling mask pointing at the Fourier coefficients
% that are to be sampled according to a given sampling strategy and a targeted
% number of samples. See [1], Sec. III.1.2 for more details about these
% strategies.
% 
% INPUTS:
%  - n1 => number of rows of the mask to generate.
%  - n2 => number of columns of the mask to generate.
%  - sampling_mode => string indicating the sampling strategy. Allowed values
%    are: 'uniform', 'uni_fc', 'gaussian', 'exponential', 'polynomial', 'star',
%    'logspiral'.
%  - M => targeted number of samples.
%  - additional inputs may be required depending on the selected sampling strategy.
%
% OUTPUTS:
%  - mask => boolean map of size 'n1 x n2' indicating the sampling locations.
%    The mask quadrants are shifted so that the DC component of the Fourier domain
%    corresponds to 'mask(1,1)'.
%  - actual_M => actual number of sampling locations (which is supposed to be as
%    closed as possible to the input parameter 'M', but strict equality may not
%    be achievable depending on the sampling strategy).
%
% Whatever the selected sampling strategy, the returned mask always present the
% following properties:
%  - the DC component is always sampled,
%  - the mask obeys a Hermitian symmetry, which is necessary to sample
%    real-valued signals in the Fourier domain.
% 
% The implemented sampling strategies are:
%  - Random uniform sampling ('uniform' code).
%  - Full sampling of the low-frequency domain + random uniform sampling of the
%    high-frequency domain ('uni_fc' code). This mode requires an additional
%    parameter to specify the cut-off frequency between the two domains.
%  - Exponential and Gaussian sampling ('exponential' and 'gaussian' codes). In
%    these modes, the probability of sampling a coefficient corresponding to
%    a spatial frequency w is given by:
% 
%                /   /  || w ||_2  \^alpha \
%     p(w) = exp | - | ----------- |       |
%                \   \     rho     /       /
%    
%    In the 'exponential' mode, the parameter alpha must be specified as an
%    additional input (while Gaussian sampling assumes alpha=2). In both cases,
%    rho is automatically adjusted to reach the targeted number of samples.
%  - Polynomial sampling ('polynomial' mode). In this mode, p(w) is defined as:
% 
%                 /       || w ||_2  \^alpha
%     p(w) = p0 * | 1 -  ----------- |
%                 \          rho     /
% 
%    Again, alpha must be specified as an additional input, while p0 is
%    automatically adjusted to reach the targeted number of samples.
%  - Star-shapped sampling ('star' mode) and log-spiral sampling ('logspiral'
%    mode). These two strategies are deterministic.
%
% AUTHOR: Yoann Le Montagner  (yoann.le-montagner -at- m4x.org) / updated
%
%
% [1] Y. Le Montagner, "Algorithmic solutions toward applications of compressed
%     sensing for optical imaging", T??l??com ParisTech, 2013.


% Coordinates in the Fourier domain
% (x,y)==(0,0) corresponds to the central frequency
sx = n2/2;
sy = n1/2;
x  = -floor(sx):1:(ceil(sx)-1);
y  = -floor(sy):1:(ceil(sy)-1);
[X, Y] = meshgrid(x, y);
THETA  = atan2(X, Y);
R      = sqrt(X.^2 + Y.^2);
rMax   = max(R(:));
fX = X./sx;
fY = Y./sy;
fR = sqrt(fX.^2 + fY.^2);

% Random map used to generate the mask
% (not used in the case of deterministic sampling modes such as 'star' or 'logspiral')
gen_map = rand(n1, n2);
gen_map2 = rand(n1,n2);
gen_map = enforce_hermitian_symmetry(gen_map);
gen_map2 = enforce_hermitian_symmetry(gen_map2);


% Uniform sampling
if(strcmpi(sampling_mode, 'uniform'))
	gen_map(R==0) = 0;
	sorted_vals = sort(gen_map(:));
	threshold   = sorted_vals(M);
	mask        = gen_map <= threshold;

% Uniform sampling with almost-full selection of the low-frequencies
elseif(strcmpi(sampling_mode, 'uni_fc_modif'))
	freq_cut = varargin{1};
    low_samp_rate = varargin{2};
	mask     = fR <= freq_cut;
    sorted_vals   = sort(gen_map(:));
	threshold     = sorted_vals(round(n1*n2*low_samp_rate));
    mask = mask & (gen_map <= threshold);
	M        = M - numel(find(mask));
	if(M>0)
        gen_map2(mask) = 2;
		sorted_vals2   = sort(gen_map2(:));
		threshold2     = sorted_vals2(M);
		mask          = mask | (gen_map2 <= threshold2);
    end
    mask = enforce_hermitian_symmetry(mask);
    
% Under sampling of the 4 images SIM simulation

elseif(strcmpi(sampling_mode, 'sim4'))
	freq_cut = varargin{1};
	disc1     = fR <= freq_cut;
    disc2     = (fY-freq_cut*cos(pi/4)).^2 + (fX-freq_cut*sin(pi/4)).^2 <= freq_cut^2 ;
    disc3     = (fY-freq_cut*cos(3*pi/4)).^2 + (fX-freq_cut*sin(3*pi/4)).^2 <= freq_cut^2 ;
    disc4     = (fY-freq_cut*cos(5*pi/4)).^2 + (fX-freq_cut*sin(5*pi/4)).^2 <= freq_cut^2 ;
    disc5     = (fY-freq_cut*cos(-pi/4)).^2 + (fX-freq_cut*sin(-pi/4)).^2 <= freq_cut^2 ;
    mask = disc1 | disc2 | disc3 | disc4 | disc5 ;
	M        = M - numel(find(mask));
	if(M>0)
		sorted_vals   = sort(gen_map(:));
		threshold     = sorted_vals(M);
		mask          = mask & (gen_map <= threshold);
    end
    mask = enforce_hermitian_symmetry(mask);

% Uniform sampling with full selection of the low-frequencies
elseif(strcmpi(sampling_mode, 'uni_fc'))
	freq_cut = varargin{1};
	mask     = fR <= freq_cut;
	M        = M - numel(find(mask));
	if(M>0)
		gen_map(mask) = 2;
		sorted_vals   = sort(gen_map(:));
		threshold     = sorted_vals(M);
		mask          = mask | (gen_map <= threshold);
    end
    mask = enforce_hermitian_symmetry(mask);
	
% Exponential and Gaussian sampling
elseif(strcmpi(sampling_mode, 'exponential') || strcmpi(sampling_mode, 'gaussian'))
	if(strcmpi(sampling_mode, 'gaussian'))
		alpha = 2;
	else
		alpha = varargin{1};
	end
	vals        = R .* power(log(1./max(1e-12, gen_map)), -1/alpha);
	sorted_vals = sort(vals(:));
	rho         = sorted_vals(M);
	mask        = vals <= rho;
    

% Polynomial sampling
elseif(strcmpi(sampling_mode, 'polynomial'))
	gen_map(R==0) = 0;
	alpha       = varargin{1};
	vals        = gen_map ./ power(1 - R/rMax, alpha);
	sorted_vals = sort(vals(:));
	p0          = sorted_vals(M);
	mask        = vals <= p0;

% Star-shapped sampling
elseif(strcmpi(sampling_mode, 'star'))
	%nb_beams = fzero(@(n) numel(find(make_star(n)))-M, [0, 500]);
    nb_beams = varargin{2} ;
	mask     = make_star(nb_beams);
    M        = M - numel(find(mask));
    if(M>0)
		gen_map(mask) = 2;
		sorted_vals   = sort(gen_map(:));
		threshold     = sorted_vals(M);
		mask          = mask | (gen_map <= threshold);
    end
    mask = enforce_hermitian_symmetry(mask);
	
% Log-spiral sampling
elseif(strcmp(sampling_mode, 'logspiral'))
	beta = fzero(@(b) numel(find(make_logspiral(b)))-M, [1e-3, 10]);
	mask = make_logspiral(beta);
	
% Unknown mode
else
	error('Unknown sampling mode: %s', sampling_mode);
end


% Re-order the sampling mask so that it can be used with FFT2
mask = ifftshift(mask);

% Actual number of samples
actual_M = numel(find(mask));



% Sampling real-valued signals in Fourier requires that the sampling mask
% obeys a Hermitian symmetry. This function enforces a Hermitian symmetry
% constaint on the map 'map' (which is supposed to have size 'n1 x n2').
function map = enforce_hermitian_symmetry(map)
	%          sep_(1)      sep_(2)      sep_(3)       sep_(4)        sep_(5)     sep_(6)
	sepx = [ 1-mod(n2,2), 2-mod(n2,2), floor(n2/2), floor(n2/2)+1, floor(n2/2)+2,   n2   ];
	sepy = [ 1-mod(n1,2), 2-mod(n1,2), floor(n1/2), floor(n1/2)+1, floor(n1/2)+2,   n1   ];
	%
	map(sepy(2):sepy(3), sepx(2):sepx(6)) = rot90 ( map(sepy(5):sepy(6), sepx(2):sepx(6)) ,2);
	map(    sepy(4)    , sepx(2):sepx(3)) = fliplr( map(    sepy(4)    , sepx(5):sepx(6)) );
	map(   1:sepy(1)   , sepx(2):sepx(3)) = fliplr( map(   1:sepy(1)   , sepx(5):sepx(6)) ); % <- only if even number of rows
	map(sepy(2):sepy(3),    1:sepx(1)   ) = flipud( map(sepy(5):sepy(6),    1:sepx(1)   ) ); % <- only if even number of columns
end


% Generate a star-shapped mask with a given number of beams.
function star_mask = make_star(nb_beams)
	nb_beams  = round(nb_beams);
	star_mask = R==0;
    theta0 = varargin{3};
	%thetas    = pi / nb_beams * (0:1:(nb_beams-1));  % regular branches
    %thetas    = pi * sort(rand(1,nb_beams));         % random branches
    thetas    = [[theta0 + pi / nb_beams * (0:1:(nb_beams-1))], [theta0:pi/360:theta0+2*pi/16]] ;  % rotated branches
	for theta=thetas
		dist      = abs(cos(theta)*X + sin(theta)*Y);
		star_mask = star_mask | (dist<=0.5);
    end
    
end


% Generate a log-spiral mask, defined by the pair of polar equations:
%
%  r = exp( beta * theta ) or r = exp( beta * (theta+pi) )
%
% (the second equation ensures that the mask is Hermitian-symmetric)
function spiral_mask = make_logspiral(beta)
	angular_dist = abs(mod(log(R)/beta - THETA + pi/2, pi) - pi/2);
	dist         = R .* angular_dist * beta/sqrt(1+beta^2);
	spiral_mask  = R==0 | (dist<=0.5);
end

end


function [x_hat, info, fig] = cs_reconstruction_2d(A, At, b, n1, n2, epsilon, regularization_mode, opts)
% CS_RECONSTRUCTION_2D Solve a CS reconstruction problem (for a 2D image).
% 
% [x_hat, info, fig] = CS_RECONSTRUCTION_2D(A, At, b, n1, n2, epsilon, regularization_mode, opts)
%
% This function solves the following optimization problem:
%
%   x_hat = argmin_x E(x) subject to || A(x) - b ||_2 <= epsilon
%
% where 'x' is a 2D image of size 'n1 x n2', and E(x) is a regularization energy
% that depends on the selected regularization mode, among:
% 
%  - 'l2'      => E(x) = || x ||_2 (this mode is "non-cs" reconstruction, but it is very fast)
%  - 'l1'      => E(x) = || x ||_1 (non-suitable for most of the natural images)
%  - 'wavelet' => E(x) = || W*x ||_1 (with W a wavelet transform operator)
%  - 'tv'      => E(x) = || x ||_TV (2D total variation)
%
% INPUTS:
%  - A => function handle, that is supposed to take a 2D image 'x' in input, and
%    return a column vector representing 'Phi*x'.
%  - At => function handle implementing the algebraic adjoint of 'A'. 'At' is
%    supposed to take a column vector in input and return a 2D image.
%  - b => column vector containing the CS measurements.
%  - n1, n2 => dimensions of the image to reconstruct.
%  - epsilon => bound on the L2 error (must be a scalar >=0).
%  - regularization_mode => see above.
%  - opts (optional) => structure containing some optional parameters. The
%    meaning of these parameters is detailed below.
%
% OPTIONS:
%  - opts.maxiter => maximum number of iterations before NESTA is stopped before
%    convergence (default: 5000).
%  - opts.tolvar => relative variation threshold used in NESTA stopping criteria
%    (default: 1e-5).
%  - opts.mu_f => smoothing parameter used in NESTA, denoted as 'mu' in [2]
%    (default: 1e-3).
%  - opts.wavename => name of the wavelet to use in the 'wavelet' regularization
%    mode. See the documentation of the function 'waveinfo' for a list a
%    available wavelets (default: 'db4').
%  - opts.wavedepth => wavelet decomposition depth in the 'wavelet' regularization
%    mode. By default, determined from the size of the image to reconstruct through
%    the following formula: floor(log2(min(n1, n2))/2) .
%  - opts.ground_truth => if provided, it is used to compute the RMSE of the
%    reconstructed image 'x_hat'.
%  - opts.verbose => string containing one or several characters among:
%     - 'r' => display the reconstructed image as well as information about
%       the reconstruction process (number of iterations, run-time, final value of the
%       objective function, RMSE if available). Icy with the Matlab X Server plugin must be ready.
%     - 'i' => show the successive iterates. This options impacts the performances of the algorithm.
%     - 'n' => show NESTA information about the successive iterates.
%    'verbose' can also be a boolean: 'true' activates everything, 'false' makes
%    the function completly silent, which is the case by default.
%  - opts.fig => if the verbosity option 'r' is activated, the reconstructed image
%    is displayed in the viewer corresponding to the ID 'fig.result'
%    (must be allocated with 'fig.result = icy_figure()'),
%    and the residue between the reconstructed image and the ground-truth is
%    displayed in the viewer corresponding to the ID 'fig.residue'. If 'i' is
%    activated, the figure handle 'fig.iterations' is used to display the
%    successive iterates (must be allocated with 'fig.iterations = figure()').
%
% OUTPUTS:
%  - x_hat => reconstructed image. This is a 2D array of size 'n1 x n2'.
%  - info => structure containing some information about the reconstruction process:
%     - info.iterations => number of iterations of the NESTA algorithm.
%     - info.runtime => execution time of the NESTA algorithm (in seconds).
%     - info.obj_func => final value of the objective function 'E(x)'
%       (depends on the selected regularization energy).
%     - info.residue => pointwise absolute difference between 'x_hat' and 'opts.ground_truth',
%       if the latter is provided.
%     - info.rmse => root mean squared error between 'x_hat' and 'opts.ground_truth',
%       if the latter is provided.
%  - fig => copy of the structure 'opts.fig', with possibly the additional fields
%    'fig.result', 'fig.residue', and 'fig.iterations' (see above).
%
%
% AUTHOR: Yoann Le Montagner  (yoann.le-montagner -at- m4x.org)
%
%
% [1] Y. Le Montagner, "Algorithmic solutions toward applications of compressed
%     sensing for optical imaging", T??l??com ParisTech, 2013.
%
% [2] S. Becker, J. Bobin, and E. Cand??s, "NESTA: a fast and accurate
%     first-order method for sparse recovery",
%     SIAM Journal of Imaging Science, vol. 4, no. 1, pp. 1-39, 2011.


% 'opts' is an optional argument
if(~exist('opts', 'var'))
	opts = struct();
end


% Default options relative to NESTA
if(~isfield(opts, 'maxiter'))
	opts.maxiter = 5000;
end
if(~isfield(opts, 'tolvar'))
	opts.tolvar = 1e-5;
end
if(~isfield(opts, 'mu_f'))
	opts.mu_f = 1e-3;
end


% Options relative to wavelet-based regularization
if(~isfield(opts, 'wavename'))
	opts.wavename = 'db4';
end
if(~isfield(opts, 'wavedepth'))
	opts.wavedepth = floor(log2(min(n1, n2)) / 2);
end


% Figures (only useful if the corresponding verbosity flags are enabled).
if(isfield(opts, 'fig') && isstruct(opts.fig))
	fig = opts.fig;
else
	fig = struct();
end


% Verbosity
if(~isfield(opts, 'verbose'))
	opts.verbose = false;
end
verbosity = struct();
verbosity.show_results    = (~ischar(opts.verbose) && opts.verbose);
verbosity.show_iterations = (~ischar(opts.verbose) && opts.verbose);
verbosity.show_info_nesta = (~ischar(opts.verbose) && opts.verbose);
if(ischar(opts.verbose))
	for c=opts.verbose
		switch(c)
			case 'r', verbosity.show_results    = true;
			case 'i', verbosity.show_iterations = true;
			case 'n', verbosity.show_info_nesta = true;
		end
	end
end


% If visualization of the iterations is requested, create the corresponding figure.
if(verbosity.show_iterations)
	if(~isfield(fig, 'iterations'))
		fig.iterations = figure();
	end
	figure(fig.iterations);
	colormap('gray');
	iter_obj_func = [];
	iter_rmse     = [];
end


% Reshape the inputs and prepare the outputs
b    = b(:);
info = struct();


% Decode the regularization mode
l2_regularization_selected = false;
switch(regularization_mode)
	case 'l2'
		l2_regularization_selected = true;
	
	case 'l1'
		nesta_mode = 'l1';
		
	case 'wavelet'
		nesta_mode = 'l1';
		
	case 'tv'
		nesta_mode = 'tv';
		
	otherwise
		error('Unknown regularization mode: %s', regularization_mode);
end


% To work with the NESTA toolbox, A and At must operate on column vectors,
% therefore some 'reshape' operations must be introduced.
B  = @(x) A(reshape(x, n1, n2));
Bt = @(y) reshape(At(y), n1*n2, 1);


% l2 reconstruction (also known as minimum energy reconstruction)
if(l2_regularization_selected)
	norm_of_b = sqrt(sum(abs(b).^2));
	x_hat     = (1 - min(1, epsilon/norm_of_b)) * Bt(b);
	info.iterations = 0;
	info.runtime    = 0;
	
% l1-based reconstruction (delegated to the NESTA toolbox)
else
	
	% Set the internal parameters of the NESTA algorithm
	opts_nesta = struct();
	opts_nesta.maxiter    = opts.maxiter; % maximum number of internal iterations
	opts_nesta.MaxIntIter = 5;            % number of continuation steps
	opts_nesta.stoptest   = 1;            % stop test based on relative change of the objective fun
	opts_nesta.TolVar     = opts.tolvar;  % stop test threshold
	
	% Verbosity
	opts_nesta.verbose = 0;
	if(verbosity.show_info_nesta)
		opts_nesta.verbose = 1;
	end
	if(verbosity.show_iterations)
		opts_nesta.outFcn = @(x) aux_show_iterations(x);
	end
	
	% Minimization mode
	opts_nesta.TypeMin = nesta_mode;
	if(strcmp(regularization_mode, 'tv'))
		opts_nesta.n1 = n1;
		opts_nesta.n2 = n2;
	elseif(strcmp(regularization_mode, 'wavelet'))
		[~, S] = wavedec2(zeros(n1, n2), opts.wavedepth, opts.wavename);
		opts_nesta.U  = @(x) aux_wavelet_dec(x);
		opts_nesta.Ut = @(w) aux_wavelet_syn(w);
	end
	
	% Run the NESTA algorithm
	t0 = tic();
	[x_hat, iterations] = NESTA(B, Bt, b, opts.mu_f, epsilon, opts_nesta);
	info.iterations = iterations;
    fprintf(['it =',num2str(iterations),'\n']);
	info.runtime    = toc(t0);
end


% Reshape the raw result to make it a 2D image
% (by default, NESTA returns a column vector).
x_hat = reshape(x_hat, n1, n2);


% Value of the objective function at the end of the reconstruction
info.obj_func = E(x_hat);


% If a ground-truth is provided, compute the residue and the RMSE.
if(isfield(opts, 'ground_truth'))
	info.residue = abs(x_hat - opts.ground_truth);
	info.rmse    = sqrt(mean(info.residue(:).^2));
end


% If requested, show the final results.
if(verbosity.show_results)
	
	% Image
	if(~isfield(fig, 'result'))
		fig.result = icy_figure();
	end
	icy_imshow(fig.result, x_hat, 'Result');
	
	% Residue
	if(isfield(info, 'residue'))
		if(~isfield(fig, 'residue'))
			fig.residue = icy_figure();
		end
		icy_imshow(fig.residue, info.residue, 'Residue');
	end
	
	% Numerical data
	if(isfield(info, 'rmse'))
		fprintf('RMSE: %f \tObj. func: %f \t#iterations: %d \trun-time: %g sec.\n', ...
			info.rmse, info.obj_func, info.iterations, info.runtime);
	else
		fprintf('Obj. func: %f \t#iterations: %d \trun-time: %g sec.\n', ...
			info.obj_func, info.iterations, info.runtime);
	end
	
end



% Wavelet decomposition
function w = aux_wavelet_dec(x)
	w = wavedec2(reshape(x, n1, n2), opts.wavedepth, opts.wavename);
	w = w(:);
end



% Wavelet synthesis
function x = aux_wavelet_syn(w)
	x = reshape(waverec2(w, S, opts.wavename), n1*n2, 1);
end



% Objective function
function f = E(im)
	switch(regularization_mode)
		case 'l2'
			f = sqrt(sum(abs(im(:)).^2));

		case 'l1'
			f = sum(abs(im(:)));

		case 'wavelet'
			w = wavedec2(im, opts.wavedepth, opts.wavename);
			f = sum(abs(w(:)));
			
		case 'tv'
			f = tv(im);
			
		otherwise
			error('Unknown regularization mode: %s', regularization_mode);
	end
end



% Called at every iteration of the NESTA algorithm and display the successive iterates.
function out = aux_show_iterations(x)
	
	% Figure
	figure(fig.iterations);
	
	% Image
	subplot(2, 2, 1);
	im = reshape(x, n1, n2);
	imagesc(im);
	title('Current estimate');
	axis off;
	
	% Objective function
	subplot(2, 2, 2);
	iter_obj_func = [iter_obj_func, E(im)];
	plot(iter_obj_func);
	title('Obj. func. = f(iteration)');
	a = axis(); a(3)=0; axis(a); % Force the min of the Y axis to be 0.
	
	if(isfield(opts, 'ground_truth'))
		
		% Residue
		subplot(2, 2, 3);
		im_residue = abs(im - opts.ground_truth);
		imagesc(im_residue);
		title('Current residue');
		axis off;
		
		% RMSE
		subplot(2, 2, 4);
		iter_rmse = [iter_rmse, sqrt(mean(im_residue(:).^2))];
		plot(iter_rmse);
		title('RMSE = f(iteration)');
		a = axis(); a(3)=0; axis(a); % Force the min of the Y axis to be 0.
	end
	
	% NESTA requires that a result is returned.
	out = 0;
end


end

function [aggregated_EWA,weight_EWA]=aggreg_EWA_adapted(fhat,rhat,beta)

% Authors A. Dalalyan and J. Salmon

% input: 
% fhat:          estimators to aggregate
% rhat:          corresponding risks estimates
% beta:          temperature parameter


% output
% aggregated_EWA: denoised 1D signal coefficients unbiased risk estimation
% weight_EWA:     associated weights



rhat_min=min(rhat);
Rand_samples = rand(size(rhat,1),1);
Rand_samples = repmat(Rand_samples,1,size(rhat,2));
weight_EWA=exp(-(rhat-rhat_min)/beta).*Rand_samples;
weight_EWA=weight_EWA./sum(weight_EWA);

aggregated_EWA=weight_EWA'*fhat;
end

function [xk,niter,residuals,outputData,opts] =NESTA(A,At,b,muf,delta,opts)
% [xk,niter,residuals,outputData] =NESTA(A,At,b,muf,delta,opts)
%
% Solves a L1 minimization problem under a quadratic constraint using the
% Nesterov algorithm, with continuation:
%
%     min_x || U x ||_1 s.t. ||y - Ax||_2 <= delta
% 
% Continuation is performed by sequentially applying Nesterov's algorithm
% with a decreasing sequence of values of  mu0 >= mu >= muf
%
% The primal prox-function is also adapted by accounting for a first guess
% xplug that also tends towards x_muf 
%
% The observation matrix A is a projector
%
% Inputs:   A and At - measurement matrix and adjoint (either a matrix, in which
%               case At is unused, or function handles).  m x n dimensions.
%           b   - Observed data, a m x 1 array
%           muf - The desired value of mu at the last continuation step.
%               A smaller mu leads to higher accuracy.
%           delta - l2 error bound.  This enforces how close the variable
%               must fit the observations b, i.e. || y - Ax ||_2 <= delta
%               If delta = 0, enforces y = Ax
%               Common heuristic: delta = sqrt(m + 2*sqrt(2*m))*sigma;
%               where sigma=std(noise).
%           opts -
%               This is a structure that contains additional options,
%               some of which are optional.
%               The fieldnames are case insensitive.  Below
%               are the possible fieldnames:
%               
%               opts.xplug - the first guess for the primal prox-function, and
%                 also the initial point for xk.  By default, xplug = At(b)
%               opts.U and opts.Ut - Analysis/Synthesis operators
%                 (either matrices of function handles).
%               opts.normU - if opts.U is provided, this should be norm(U)
%                   otherwise it will have to be calculated (potentially
%                   expensive)
%               opts.MaxIntIter - number of continuation steps.
%                 default is 5
%               opts.maxiter - max number of iterations in an inner loop.
%                 default is 10,000
%               opts.TolVar - tolerance for the stopping criteria
%               opts.stopTest - which stopping criteria to apply
%                   opts.stopTest == 1 : stop when the relative
%                       change in the objective function is less than
%                       TolVar
%                   opts.stopTest == 2 : stop with the l_infinity norm
%                       of difference in the xk variable is less
%                       than TolVar
%               opts.TypeMin - if this is 'L1' (default), then
%                   minimizes a smoothed version of the l_1 norm.
%                   If this is 'tv', then minimizes a smoothed
%                   version of the total-variation norm.
%                   The string is case insensitive.
%               opts.n1 - height of the input images (used only for
%                   TV-minimization and TV3D-minimization).
%               opts.n2 - width of the input images (used only for
%                   TV-minimization and TV3D-minimization).
%               opts.nT - number of frames of the input sequences
%                   (used only for TV3D-minimization).
%               opts.Verbose - if this is 0 or false, then very
%                   little output is displayed.  If this is 1 or true,
%                   then output every iteration is displayed.
%                   If this is a number p greater than 1, then
%                   output is displayed every pth iteration.
%               opts.fid - if this is 1 (default), the display is
%                   the usual Matlab screen.  If this is the file-id
%                   of a file opened with fopen, then the display
%                   will be redirected to this file.
%               opts.errFcn - if this is a function handle,
%                   then the program will evaluate opts.errFcn(xk)
%                   at every iteration and display the result.
%                   ex.  opts.errFcn = @(x) norm( x - x_true )
%               opts.outFcn - if this is a function handle, 
%                   then then program will evaluate opts.outFcn(xk)
%                   at every iteration and save the results in outputData.
%                   If the result is a vector (as opposed to a scalar),
%                   it should be a row vector and not a column vector.
%                   ex. opts.outFcn = @(x) [norm( x - xtrue, 'inf' ),...
%                                           norm( x - xtrue) / norm(xtrue)]
%               opts.AAtinv - this is an experimental new option.  AAtinv
%                   is the inverse of AA^*.  This allows the use of a 
%                   matrix A which is not a projection, but only
%                   for the noiseless (i.e. delta = 0) case.
%               opts.USV - another experimental option.  This supercedes
%                   the AAtinv option, so it is recommended that you
%                   do not define AAtinv.  This allows the use of a matrix
%                   A which is not a projection, and works for the
%                   noisy ( i.e. delta > 0 ) case.
%                   opts.USV should contain three fields: 
%                   opts.USV.U  is the U from [U,S,V] = svd(A)
%                   likewise, opts.USV.S and opts.USV.V are S and V
%                   from svd(A).  S may be a matrix or a vector.
%
%  Outputs:
%           xk  - estimate of the solution x
%           niter - number of iterations
%           residuals - first column is the residual at every step,
%               second column is the value of f_mu at every step
%           outputData - a matrix, where each row r is the output
%               from opts.outFcn, if supplied.
%           opts - the structure containing the options that were used    
%
% Written by: Jerome Bobin, Caltech
% Email: bobin@acm.caltech.edu
% Created: February 2009
% Modified (version 1.0): May 2009, Jerome Bobin and Stephen Becker, Caltech
% Modified (version 1.1): Nov 2009, Stephen Becker, Caltech
% Modified (unofficial version): December 2013, Yoann Le Montagner.
%
% NESTA Version 1.1
%   See also Core_Nesterov


if nargin < 6, opts = []; end
if isempty(opts) && isnumeric(opts), opts = struct; end

%---- Set defaults
fid = setOpts('fid',1);
Verbose = setOpts('Verbose',true);
function printf(varargin), fprintf(fid,varargin{:}); end
MaxIntIter = setOpts('MaxIntIter',5,1);
TypeMin = setOpts('TypeMin','L1');
TolVar = setOpts('tolvar',1e-5);
[U,U_userSet] = setOpts('U', @(x) x );
if ~isa(U,'function_handle')
    Ut = setOpts('Ut',[]);
else
    Ut = setOpts('Ut', @(x) x );
end
xplug = setOpts('xplug',[]);
normU = setOpts('normU',[]);  % so we can tell if it's been set

residuals = []; outputData = [];
AAtinv = setOpts('AAtinv',[]);
USV = setOpts('USV',[]);
if ~isempty(USV)
    if isstruct(USV)
        Q = USV.U;  % we can't use "U" as the variable name
                    % since "U" already refers to the analysis operator
        S = USV.S;
        if isvector(S), s = S; %S = diag(s);
        else s = diag(S); end
        %V = USV.V;
    else
        error('opts.USV must be a structure');
    end
end

% -- We can handle non-projections IF a (fast) routine for computing
%    the psuedo-inverse is available.
%    We can handle a nonzero delta, but we need the full SVD
if isempty(AAtinv) && isempty(USV)
	
	%--- Begin patch YLM
	
	% % Check if A is a partial isometry, i.e. if AA' = I
	% z = randn(size(b));
	% if isa(A,'function_handle'), AAtz = A(At(z));
	% else AAtz = A*(A'*z); end
	% if norm( AAtz - z )/norm(z) > 1e-8
	% 	error('Measurement matrix A must be a partial isometry: AA''=I');
	% end
	
	% Check if (A'A)^2 = A'A, i.e. A'A is a projector
	% (this is more general than checking that AA' = I).
	if isa(A,'function_handle')
		ylm_Atb = At(b);
	else
		ylm_Atb = At*b;
	end
	ylm_z = randn(size(ylm_Atb));
	if isa(A,'function_handle')
		ylm_AtAz    = At(A(ylm_z));
		ylm_AtAAtAz = At(A(ylm_AtAz));
	else
		ylm_AtAz    = At*A*ylm_z;
		ylm_AtAAtAz = At*A*ylm_AtAz;
	end
	if(norm(ylm_AtAAtAz-ylm_AtAz)/norm(ylm_z) > 1e-8)
		error('Measurement matrix A must be such that A''A is a projector: (A''A)^2=I');
	end
	
	%--- End patch YLM
	
end

% -- Find a initial guess if not already provided.
%   Use least-squares solution: x_ref = A'*inv(A*A')*b
% If A is a projection, the least squares solution is trivial
if isempty(xplug) || norm(xplug) < 1e-12
    if ~isempty(USV) && isempty(AAtinv)
        AAtinv = Q*diag( s.^(-2) )*Q';
    end
    if ~isempty(AAtinv)
        if delta > 0 && isempty(USV)
            error('delta must be zero for non-projections');
        end
        if isa(AAtinv,'function_handle')
            x_ref = AAtinv(b);
        else
            x_ref = AAtinv * b;
        end
    else
        x_ref = b;
    end
    
    if isa(A,'function_handle')
        x_ref=At(x_ref);
    else
        x_ref = A'*x_ref;
    end

    if isempty(xplug)
        xplug = x_ref;
    end
    % x_ref itself is used to calculate mu_0
    %   in the case that xplug has very small norm
else
    x_ref = xplug;
end

% use x_ref, not xplug, to find mu_0
if isa(U,'function_handle')
    Ux_ref = U(x_ref);
else
    Ux_ref = U*x_ref;
end
switch lower(TypeMin)
	case 'l1'
		mu0 = 0.9*max(abs(Ux_ref));
	case 'tv'
		mu0 = ValMUTv(Ux_ref);
		
	%--- Begin patch YLM
	
	case 'tv3d'
		mu0 = ValMUTv3D(Ux_ref);
	
	%--- End patch YLM
	
end

% -- If U was set by the user and normU not supplied, then calcuate norm(U)
if U_userSet && isempty(normU)
    % simple case: U*U' = I or U'*U = I, in which case norm(U) = 1
    z = randn(size(xplug));
    if isa(U,'function_handle'), UtUz = Ut(U(z)); else UtUz = U'*(U*z); end
    if norm( UtUz - z )/norm(z) < 1e-8
        normU = 1;
    else
        z = randn(size(Ux_ref));
        if isa(U,'function_handle')
            UUtz = U(Ut(z)); 
        else
            UUtz = U*(U'*z);
        end
        if norm( UUtz - z )/norm(z) < 1e-8
            normU = 1;
        end
    end
    
    if isempty(normU)
        % have to actually calculate the norm
        if isa(U,'function_handle')
            [normU,cnt] = my_normest(U,Ut,length(xplug),1e-3,30);
            if cnt == 30, printf('Warning: norm(U) may be inaccurate\n'); end
        else
            [mU,nU] = size(U);
            if mU < nU, UU = U*U'; else UU = U'*U; end 
            % last resort is to call MATLAB's "norm", which is slow
            if norm( UU - diag(diag(UU)),'fro') < 100*eps
                % this means the matrix is diagonal, so norm is easy:
                normU = sqrt( max(abs(diag(UU))) );
            elseif issparse(UU)
                normU = sqrt( normest(UU) );
            else
                if min(size(U)) > 2000
                    % norm(randn(2000)) takes about 5 seconds on my PC
                    printf('Warning: calculation of norm(U) may be slow\n');
                end
                normU = sqrt( norm(UU) );
            end
        end
    end
    opts.normU = normU;
end
        

niter = 0;
Gamma = (muf/mu0)^(1/MaxIntIter);
mu = mu0;
Gammat= (TolVar/0.1)^(1/MaxIntIter);
TolVar = 0.1;
 
for nl=1:MaxIntIter
    
    mu = mu*Gamma;
    TolVar=TolVar*Gammat;    opts.TolVar = TolVar;
    opts.xplug = xplug;
    if Verbose, printf('\tBeginning %s Minimization; mu = %g\n',opts.TypeMin,mu); end
    [xk,niter_int,res,out,optsOut] = Core_Nesterov(...
        A,At,b,mu,delta,opts);
    
    xplug = xk;
    niter = niter_int + niter;
    
    residuals = [residuals; res];
    outputData = [outputData; out];

end
opts = optsOut;


%---- internal routine for setting defaults
function [var,userSet] = setOpts(field,default,mn,mx)
    var = default;
    % has the option already been set?
    if ~isfield(opts,field) 
        % see if there is a capitalization problem:
        names = fieldnames(opts);
        for i = 1:length(names)
            if strcmpi(names{i},field)
                opts.(field) = opts.(names{i});
                opts = rmfield(opts,names{i});
                break;
            end
        end
    end
    if isfield(opts,field) && ~isempty(opts.(field))
        var = opts.(field);  % override the default
        userSet = true;
    else
        userSet = false;
    end
    % perform error checking, if desired
    if nargin >= 3 && ~isempty(mn)
        if var < mn
            printf('Variable %s is %f, should be at least %f\n',...
                field,var,mn); error('variable out-of-bounds');
        end
    end
    if nargin >= 4 && ~isempty(mx)
        if var > mx
            printf('Variable %s is %f, should be at least %f\n',...
                field,var,mn); error('variable out-of-bounds');
        end
    end
    opts.(field) = var;
end




%---- internal routine for setting mu0 in the tv minimization case
function th=ValMUTv(x)

	N = length(x);
	
	%--- Begin patch YLM
	
	% n = floor(sqrt(N));
	
	% Dv = spdiags([reshape([-ones(n-1,n); zeros(1,n)],N,1) ...
	% 	reshape([zeros(1,n); ones(n-1,n)],N,1)], [0 1], N, N);
	% Dh = spdiags([reshape([-ones(n,n-1) zeros(n,1)],N,1) ...
	% 	reshape([zeros(n,1) ones(n,n-1)],N,1)], [0 n], N, N);
	
	% Read the dimensions of the input image from opts.n1 and opts.n2. If those
	% fields are not available, assume that the image is square (this is the
	% original NESTA behavior).
	if(isfield(opts, 'n1') && isfield(opts, 'n2'))
		n1 = opts.n1;
		n2 = opts.n2;
	else
		n1 = floor(sqrt(N));
		n2 = n1;
	end
	
	% Derivation operators
	Dv = spdiags([ ...
		reshape(cat(1, -ones(n1-1,n2), zeros(   1,n2)), N, 1), ...
		reshape(cat(1, zeros(   1,n2), ones (n1-1,n2)), N, 1)  ...
	], [0 1], N, N);
	Dh = spdiags([ ...
		reshape(cat(2, -ones(n1,n2-1), zeros(n1,   1)), N, 1), ...
		reshape(cat(2, zeros(n1,   1), ones (n1,n2-1)), N, 1)  ...
	], [0 n1], N, N);
	
	%--- End patch YLM
	

	%D = sparse([Dh;Dv]);

	Dhx = Dh*x;
	Dvx = Dv*x;

	sk = sqrt(abs(Dhx).^2 + abs(Dvx).^2);
	th = max(sk);

end




%--- Begin patch YLM

%---- internal routine for setting mu0 in the tv 3D minimization case
function th=ValMUTv3D(x)
	
	% The dimensions of the sequence must be specified when using the TV3D mode.
	n1 = opts.n1;
	n2 = opts.n2;
	nT = opts.nT;
	N  = n1*n2*nT;
	
	% Derivation operators
	Dv = spdiags([ ...
		reshape(cat(1, -ones(n1-1,n2,nT), zeros(   1,n2,nT)), N, 1), ...
		reshape(cat(1, zeros(   1,n2,nT), ones (n1-1,n2,nT)), N, 1)  ...
	], [0 1], N, N);
	Dh = spdiags([ ...
		reshape(cat(2, -ones(n1,n2-1,nT), zeros(n1,   1,nT)), N, 1), ...
		reshape(cat(2, zeros(n1,   1,nT), ones (n1,n2-1,nT)), N, 1)  ...
	], [0 n1], N, N);
	Ds = spdiags([ ...
		reshape(cat(3, -ones(n1,n2,nT-1), zeros(n1,n2,   1)), N, 1), ...
		reshape(cat(3, zeros(n1,n2,   1), ones (n1,n2,nT-1)), N, 1)  ...
	], [0 n1*n2], N, N);
	
	Dhx = Dh*x;
	Dvx = Dv*x;
	Dsx = Ds*x;
	
	sk = sqrt(abs(Dhx).^2 + abs(Dvx).^2 + abs(Dsx).^2);
	th = max(sk);
	
end

%--- End patch YLM



end %-- end of NESTA function

%%%%%%%%%%%% POWER METHOD TO ESTIMATE NORM %%%%%%%%%%%%%%%
% Copied from MATLAB's "normest" function, but allows function handles, not just sparse matrices
function [e,cnt] = my_normest(S,St,n,tol, maxiter)
%MY_NORMEST Estimate the matrix 2-norm via power method.
    if nargin < 4, tol = 1.e-6; end
    if nargin < 5, maxiter = 20; end
    if isempty(St)
        St = S;  % we assume the matrix is symmetric;
    end
    x = ones(n,1);
    cnt = 0;
    e = norm(x);
    if e == 0, return, end
    x = x/e;
    e0 = 0;
    while abs(e-e0) > tol*e && cnt < maxiter
       e0 = e;
       Sx = S(x);
       if nnz(Sx) == 0
          Sx = rand(size(Sx));
       end
       e = norm(Sx);
       x = St(Sx);
       x = x/norm(x);
       cnt = cnt+1;
    end
end

end
